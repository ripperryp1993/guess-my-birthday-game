from random import randint

guest_name = input('Hi! what is your name?\n')


for guess_number in range(1,6):
    guess_year = randint(1924, 2004)
    guess_month = randint(1, 12)
    
    print('Guess', guess_number, ':', guest_name, 
    'were you born on', guess_month, '/', guess_year, '?')
    answer = input('yes or no?\n')
    
    if (answer == 'yes'):
        print('I knew it!')
        exit()
    elif (answer == 'no'): 
        print('Drat! Lemme try again!')
    else:
        print('Sorry, it is an invalid answer. Please try again!')

print("I have other things to do. Good bye.")

